package Homework8_inheritance;

public class AndroidApp extends App{

    public AndroidApp(String name) {
        super(name);
    }
    public void runAndroidApp(){
        System.out.println("App "+ name + " is running on Android phone");
    }
}
