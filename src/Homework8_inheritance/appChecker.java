package Homework8_inheritance;

public class appChecker {
    public static void main(String[] args) {
    AndroidApp appForLG = new AndroidApp("Android application");
    IphoneApp appForIphone = new IphoneApp("IOS application");
    appForLG.runAndroidApp();
    appForLG.appInfo();
    appForIphone.runIphoneApp();
    appForIphone.appInfo();
    }

}
