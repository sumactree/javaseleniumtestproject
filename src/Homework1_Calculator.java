import java.util.Scanner;
public class Homework1_Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwie liczby");
        int firstNumber = scanner.nextInt();
        int secondNumber = scanner.nextInt();
        int addition = firstNumber + secondNumber;
        int subtraction = firstNumber - secondNumber;
        int multiplication = firstNumber * secondNumber;
        int division = firstNumber / secondNumber;

        System.out.println("addition:" + addition);
        System.out.println("subtraction:" + subtraction);
        System.out.println("multiplication:" + multiplication);
        System.out.println("division:" + division);
    }
}
