package Homework12_Palindrom;

public class Palindrom {
    public static void main(String[] args) {
//        part of solution from the course:
//        System.out.println(isPalidrom("kajak"));
        String palindrom = "kajak";
        boolean isPalindrome = true;
        char[] palindromArray = palindrom.toCharArray();
        for (int i = 0; i < (palindrom.length()) / 2; i++) {
            if (palindromArray[i] != palindromArray[palindrom.length() - 1 - i]) {
                isPalindrome = false;
                break;
            }
        }
        if (isPalindrome) {
            System.out.println(palindrom + " is a palindrome");
        } else {
            System.out.println(palindrom + " is not a palindrome");
        }
    }
//    part of solution from the course :
//    public static boolean isPalidrom (String word){
//        int n = word.length();
//        for (int i=0; i < n/2; i++){
//            if (word.charAt(i) != word.charAt(n-i-1)){
//                return false;
//            }
//        }
//        return true;
//    }
}

