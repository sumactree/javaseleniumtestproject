import java.util.Scanner;
import java.util.Random;

public class MyHomework1_RockPaperScissors {
    public static void main(String[] args) {
        playGame();
    }

    public static void playGame() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type one of the options : paper, rock or scissors");
        String usersTurn = scanner.nextLine();
        if (!usersTurn.equals("paper") && !usersTurn.equals("rock") && !usersTurn.equals("scissors")) {
            System.out.println("You didn't type one of the options : paper, rock or scissors. Try again");
        } else {
            String[] gameOptions = new String[]{"paper", "rock", "scissors"};
            String computersTurn = gameOptions[new Random().nextInt(gameOptions.length)];
            System.out.println(computersTurn);
            if (usersTurn.equals(computersTurn)) {
                System.out.println("It's a draw. Try again");
            } else if (usersTurn.equals("paper") && computersTurn.equals("rock")) {
                System.out.println("You win! Great job");
            } else if (usersTurn.equals("paper") && computersTurn.equals("scissors")) {
                System.out.println("You lost. Try again");
            } else if (usersTurn.equals("rock") && computersTurn.equals("scissors")) {
                System.out.println("You win! Great job");
            } else if (usersTurn.equals("rock") && computersTurn.equals("paper")) {
                System.out.println("You lost. Try again");
            } else if (usersTurn.equals("scissors") && computersTurn.equals("paper")) {
                System.out.println("You win! Great job");
            } else if (usersTurn.equals("scissors") && computersTurn.equals("rock")) {
                System.out.println("You lost. Try again");
            }
        }
    }
}



