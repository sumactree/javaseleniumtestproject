package Homework6_changeCalculator;

import java.util.Scanner;

public class CalculatorTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwie liczby");
        int firstNumber = scanner.nextInt();
        int secondNumber = scanner.nextInt();
        Calculator numbers = new Calculator();

        System.out.println(numbers.addition(firstNumber,secondNumber));
        System.out.println(numbers.subtraction(firstNumber,secondNumber));
        System.out.println(numbers.multiplication(firstNumber,secondNumber));
        System.out.println(numbers.division(firstNumber,secondNumber));
    }
}
