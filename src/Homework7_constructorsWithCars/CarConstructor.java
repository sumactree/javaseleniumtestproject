package Homework7_constructorsWithCars;

public class CarConstructor {
    public String brand;
    public String model;
    public int age;
    public int carMileage;
    public CarConstructor(String brand, String model, int age, int carMileage) {
        this.brand = brand;
        this.model = model;
        this.age = age;
        this.carMileage = carMileage;
    }
    public void go(){
        System.out.println("Go for adventure with your car " + brand + " " + model + " " + age + " " + carMileage);
    }
}
