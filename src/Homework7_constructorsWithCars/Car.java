package Homework7_constructorsWithCars;

public class Car {
    public static void main(String[] args) {
    CarConstructor auto1 = new CarConstructor("Mercedes","model 5", 2020, 1000);
    CarConstructor auto2 = new CarConstructor("Audi","A5", 2021, 2000);
    auto1.go();
    auto2.go();
    }
}
