package Homework10_exceptions;

import Homework9_interface.NoValidBrowserName;

import java.util.Scanner;

public class AgeCheckerExceptions {
    public static void main(String[] args) throws InvalidAgeException{
        Scanner scanner = new Scanner(System.in);
        System.out.println("How old are you?");
        int Age = scanner.nextInt();
        if(Age>=18) {
            System.out.println("Thank you for shopping");
        } else if (Age<0){
            System.out.println("Please enter a valid positive number");
            throw new InvalidAgeException("User must be more than 0 years old");
        } else {
            System.out.println("You can't buy alcohol. You are too young");
            throw new InvalidAgeException(" ");
        }
    }
}
