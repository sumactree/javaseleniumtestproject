package Homework9_interface;

public class NoValidBrowserName extends Exception{
    public NoValidBrowserName(String message) {
        super(message);
    }
}
