package Homework9_interface;

public class ChromeDriver implements Webdriver{

    @Override
    public void get() {
        System.out.println("I open the Chrome browser");
    }

    @Override
    public void findElement() {
        System.out.println("It finds element in the Chrome browser");
    }
}
