package Homework9_interface;

public class WebDriverTest {
    public static void main(String[] args) throws NoValidBrowserName {
        Webdriver driver = getDriver("chromeq");
        driver.get();
        driver.findElement();
//        ChromeDriver chrome = new ChromeDriver();
//        FirefoxDriver firefox = new FirefoxDriver();
//        chrome.get();
//        chrome.findElement();
//        firefox.get();
//        firefox.findElement();
    }

    private static Webdriver getDriver(String name) throws NoValidBrowserName {
        if (name.equals("chrome")){
            return new ChromeDriver();
        }else if (name.equals("firefox")){
            return new FirefoxDriver();
        }
        throw new NoValidBrowserName("There are no correct browser");
    }
}
