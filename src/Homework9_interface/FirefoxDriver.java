package Homework9_interface;

public class FirefoxDriver implements Webdriver {
    @Override
    public void get() {
        System.out.println("I open the Firefox browser");
    }

    @Override
    public void findElement() {
        System.out.println("It finds element in the Firefox browser");
    }
}
