import java.util.Scanner;

public class Homework2_AgeChecker {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How old are you?");
        int Age = scanner.nextInt();
        if(Age>=18) {
            System.out.println("Thank you for shopping");
        } else if (Age<0) {
            System.out.println("Please enter a valid positive number");
        } else {
            System.out.println("You can't buy alcohol. You are too young");
        }
    }
}
