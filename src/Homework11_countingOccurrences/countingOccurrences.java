package Homework11_countingOccurrences;
import java.util.HashMap;
import java.util.Map;

public class countingOccurrences {
    static int[] numbers = new int[] {1,2,2,3,4,5,3,3};
    public static void main(String[] args) {
         numberOfOccurrences();
    }
    private static void numberOfOccurrences() {
        Map<Integer, Integer> occurrences = new HashMap<>();
        for (int key : numbers) {
            if(occurrences.containsKey(key)){
                Integer value = occurrences.get(key);
                System.out.println(value);
                occurrences.put(key, value +1);
            }else {
                occurrences.put(key,1);
            }
        }
        for(Integer key1 : occurrences.keySet()){
            System.out.println("The number " + key1 + " occurs " + occurrences.get(key1));
        }
    }
}
