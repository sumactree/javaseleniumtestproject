package Homework5_students;

public class informationAboutStudents {
    public String name;
    public String surname;
    public String nick;
    public String email;
    public int indexNumber;

    public void whatsYourName(){
        System.out.println("My name is " + name + surname);
    }
    public void logIn(){
        System.out.println("Log in by " + nick);
    }
    public void typeIndexNumber(){
        System.out.println("Type Index number " + indexNumber);
    }
    public void typeEmail(){
        System.out.println("Type E-mail " + email);
    }

}
