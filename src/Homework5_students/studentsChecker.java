package Homework5_students;

public class studentsChecker {
    public static void main(String[] args) {
        informationAboutStudents John = new informationAboutStudents();
        John.name = "John";
        John.surname = "Smith";
        John.nick = "JoSmi";
        John.email = "johnsmith@gmail.com";
        John.indexNumber = 12345;

        informationAboutStudents Kelly = new informationAboutStudents();
        Kelly.name = "Kelly";
        Kelly.surname = "Flower";
        Kelly.nick = "KelFlo";
        Kelly.email = "kellyflower@gmail.com";
        Kelly.indexNumber = 54321;

        informationAboutStudents [] students = new informationAboutStudents[] {John,Kelly};
        for (int i=0; i< students.length; i++ ){
            students[i].whatsYourName();
            students[i].logIn();
            students[i].typeEmail();
            students[i].typeIndexNumber();
        }

    }

}
