package Homework13_Fibonacci;

public class Fibonacci {
    public static void main(String[] args) {
        int n = 30;
        int[] fibo = new int[n];
        fibo[0] = 0;
        fibo[1] = 1;
        FibonacciSequence(fibo, n);
    }
    public static void FibonacciSequence(int[] fibo, int n) {
        for (int i = 2; i < n; i++) {
            fibo[i] = fibo[i - 1] + fibo[i - 2];
        }
        System.out.println(fibo[n-1]);
    }
}