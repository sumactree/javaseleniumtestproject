import java.util.Arrays;
public class Homework4_InverseArray {
    public static void main(String[] args) {
        int[] array = new int[] {1,3,5,2,6};
        int[] reversed = new int[5];
        for (int i = array.length - 1; i >= 0; i--){
            reversed[array.length - 1 - i] = array[i];
        }
        System.out.println(Arrays.toString(reversed));

// The solution from the course:
        int[] array1 = new int[]{1, 3, 5, 2, 7};
        for (int i = 0; i < (array1.length / 2); i++) {
            int temp = array1[i]; // 1 //3
            array1[i] = array1[array1.length - 1 - i]; // 7 //2
            array1[array1.length - 1 - i] = temp;
        }
        for (int i = 0; i < array1.length; i++) {
            System.out.println(array1[i]);
        }
    }
}
